FROM openjdk:8-jre
LABEL image.name="epicHTTPS" \
      image.description="epicHTPS provides a graphical interface for managing SSL certificates" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2017 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV VERSION="latest" \
    SPRING_PROFILES_ACTIVE="prod,nginx" \
    EPICHTTPS_USERNAME="admin" \
    EPICHTTPS_PASSWORD="password" \
    EPICHTTPS_ENCRYPTED="false" \
    EPICHTTPS_HOSTPATH="/etc/letsencrypt" \
    EPICHTTPS_DOMAIN="epichttps.example.com" \
    JAVA_OPTS=""

# system upgrade
RUN apt-get -y update \
 && apt-get -y upgrade \
 # install installations requirements
 && apt-get -y install git \
 # install runtime requirements
 && apt-get -y install openssl \
                       curl \
 # install letsencrypt
 && git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt \
 && /opt/letsencrypt/letsencrypt-auto --os-packages-only \
 && /opt/letsencrypt/letsencrypt-auto --version \
 # cleanup installations requirements and apt
 && apt-get -y purge git \
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://bitbucket.org/epicsoft/epichttps/downloads/epichttps-$VERSION.jar /app.jar

EXPOSE 8080

VOLUME ["/etc/letsencrypt"]

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]